const dedent = require('dedent');
const hyperstream = require('hstream');

function document() {
  return hyperstream({
    head: {
      _prependHtml: dedent`
      <link rel="manifest" href="manifest.json">
      <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.0/dist/chart.min.js" integrity="sha256-Y26AMvaIfrZ1EQU49pf6H4QzVTrOI8m9wQYKkftBt4s=" crossorigin="anonymous"></script>
      `,
      _appendHtml: dedent`
        <link rel="shortcut icon" href="/favicon.ico">
      `
    }
  });
}

module.exports = document;
