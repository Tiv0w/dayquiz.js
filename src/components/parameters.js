const html = require('choo/html');

module.exports = function(state, emit) {
  const callback = () => { console.log('On change la valeur'); emit('toggleTime'); };
  return html`
    <div>
      <div>
        <label class="pa4 ma4">
          <input class="pa2 ma2" type="checkbox" onchange=${callback} checked=${state.isTimed}>
          Chronométré
        </label>
      </div>
    </div>
`;
};
