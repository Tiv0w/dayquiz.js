const html = require('choo/html');

module.exports = function(state, emit) {
  const dayClicked = (dayNumber) => () => {
    if (state.isWin === null) emit('clickDay', dayNumber);
  };
  return html`
    <div class="answers-div">
       <button onclick=${dayClicked(1)}>Lundi</button>
       <button onclick=${dayClicked(2)}>Mardi</button>
       <button onclick=${dayClicked(3)}>Mercredi</button>
       <button onclick=${dayClicked(4)}>Jeudi</button>
       <button onclick=${dayClicked(5)}>Vendredi</button>
       <button onclick=${dayClicked(6)}>Samedi</button>
       <button onclick=${dayClicked(0)}>Dimanche</button>
    </div>
  `;
};
