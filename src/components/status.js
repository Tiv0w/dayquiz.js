const html = require('choo/html');

const numberToDay = [
  'Dimanche',
  'Lundi',
  'Mardi',
  'Mercredi',
  'Jeudi',
  'Vendredi',
  'Samedi'
];

function replayButton(emit) {
  return html`
    <button id="replay-button" onclick=${() => emit('replay')}>
      Rejouer
    </button>
  `;
}

function timeDisplay(timeTaken) {
  const timeTakenMillis = Number(timeTaken % 100).toPrecision(2);
  const timeTakenSeconds = Math.floor((timeTaken / 1000) % 60);
  const timeTakenMinutes = Math.floor(timeTaken / 60000);

  return html`
    <div>
      ${timeTakenMinutes > 0 ? timeTakenMinutes + 'min' : ''} ${timeTakenSeconds}sec ${timeTakenMillis}
    </div>
  `;
}

module.exports = function(state, emit) {
  if (state.isWin === true) {
    return html`
      <div>
        <div>Correct ! 🥳</div>
        ${state.isTimed ? timeDisplay(state.timeTaken) : null}
        <div class="replay-button-container">${replayButton(emit)}</div>
      </div>
    `;
  } else if (state.isWin === false) {
    const currentDay = state.currentDate.getDay();
    const currentDayString = numberToDay[currentDay];
    return html`
      <div>
        <div>Faux. C'est un ${currentDayString} (${currentDay}).</div>
        ${state.isTimed ? timeDisplay(state.timeTaken) : null}
        <div class="replay-button-container">${replayButton(emit)}</div>
      </div>
    `;
  }
};
