const html = require('choo/html');

module.exports = function(state, emit) {
  return html`
    <canvas id="chart" width="600" height="200"></canvas>
  `
};
