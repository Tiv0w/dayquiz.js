const choo = require('choo');
const css = require('sheetify');
const main = require('./views/main');
const store = require('./store');

css('tachyons');
css('../style.css');

const app = choo();
if (process.env.NODE_ENV !== 'production') {
  app.use(require('choo-devtools')());
} else {
  app.use(require('choo-service-worker')());
}

app.use(store);

app.route('/', main);

module.exports = app.mount('body');
