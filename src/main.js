const html = require('choo/html');
const parameters = require('./components/parameters');
const answers = require('./components/answers');
const status = require('./components/status');


module.exports = function(state, emit) {
  return html`
    <div class="container">
      <div id="date-display">
        ${state.currentDate.toLocaleDateString('fr-FR')}
      </div>
      <div id="answers">
        ${answers(state, emit)}
      </div>
      <div id="status">
        ${status(state, emit)}
      </div>
      <div id="parameters">
        ${parameters(state)}
      </div>
    </div>
  `;
};
