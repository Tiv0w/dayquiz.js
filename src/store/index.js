const { generateDate, verifyAnswer } = require('../logic');
const storage = require('../storage');
const { performance } = require('perf_hooks');

function handleClickDay(state, emitter, dayNumber) {
  state.dayClicked = dayNumber;
  state.isWin = verifyAnswer(state.currentDate, dayNumber);
  if (state.isTimed) {
    state.endTime = performance.now();
    state.timeTaken = state.endTime - state.startTime;
  }
  state.results.push({
    status: state.isWin,
    time: state.isTimed ? state.timeTaken : null
  });
  storage.setObject('results', state.results);
  emitter.emit('render');
}

function handleReplay(state, emitter) {
  state.dayClicked = null;
  state.isWin = null;
  state.currentDate = generateDate(1800, 2100);
  state.startTime = performance.now();
  emitter.emit('render');
}

function handleToggleTime(state, emitter) {
  state.isTimed = !state.isTimed;
  // emitter.emit('render');
}

module.exports = function(state, emitter) {
  // initial state
  state.currentDate = generateDate(1800, 2100);
  state.dayClicked = null;
  state.isWin = null;
  state.isTimed = false;
  state.results = storage.getObject('results') || [];
  state.startTime = performance.now();

  emitter.on('clickDay', dayNumber => handleClickDay(state, emitter, dayNumber));
  emitter.on('replay', () => handleReplay(state, emitter));
  emitter.on('toggleTime', () => handleToggleTime(state, emitter));
};
