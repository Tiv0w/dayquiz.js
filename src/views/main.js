const html = require('choo/html');
const parameters = require('../components/parameters');
const answers = require('../components/answers');
const status = require('../components/status');
const statistics = require('../components/statistics');

const TITLE = 'Dayquiz';

module.exports = function(state, emit) {
  if (state.title !== TITLE) emit(state.events.DOMTITLECHANGE, TITLE);
  return html`
    <body>
      <div class="container">
        <div id="date-display">
          ${state.currentDate.toLocaleDateString('fr-FR')}
        </div>
        <div id="answers">
          ${answers(state, emit)}
        </div>
        <div id="status">
          ${status(state, emit)}
        </div>
        <div id="parameters">
          ${parameters(state, emit)}
        </div>
        <div id="stats">
          ${statistics(state, emit)}
        </div>
      </div>
    </body>
  `
};
