function generateDate(lowerBoundYear, upperBoundYear) {
  const lowerBoundTimestamp = new Date(lowerBoundYear, 0, 1).valueOf();
  const upperBoundTimestamp = new Date(upperBoundYear, 11, 31).valueOf();
  const timeDiff = upperBoundTimestamp - lowerBoundTimestamp;

  const chosenTimestamp = Math.random() * timeDiff + lowerBoundTimestamp;
  return new Date(chosenTimestamp);
}

function verifyAnswer(currentDate, answer) {
  const correctAnswer = currentDate.getDay();
  return (correctAnswer === answer % 7);
}

module.exports = {
  generateDate,
  verifyAnswer
};
