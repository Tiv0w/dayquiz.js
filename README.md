# Dayquiz.js

("Quiztidien" in french)

Little rewrite project to get a [dayquiz](https://gitlab.com/Tiv0w/dayquiz) running in the browser.

Made with [choo](https://github.com/choojs/choo).


#### Development notes

- [x] Rewrite the core
  - [x] Add the "question".
  - [x] Add the guesses buttons.
  - [x] Add the answer display.
- [ ] Fix timer style.
- [ ] Add timed parameter.
- [ ] Use local storage to store results.
- [ ] Display stats.


### License
Copyright © 2022 Tiv0w
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
